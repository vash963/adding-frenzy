export default class Operation {
    constructor(first, second, id, type){
        this.id = id
        this.first = first
        this.second = second
        this.type = type
    }

    checkResult(result){
        if(this.type == 'sum'){
            return this.checkSum(result)
        } else if (this.type == 'subtraction'){
            return this.checkSubtraction(result)
        } else if (this.type == 'multiplication'){
            return this.checkMultiplication(result)
        }
    }
    getSign(result){
        if(this.type == 'sum'){
            return '+'
        } else if (this.type == 'subtraction'){
            return '-'
        } else if (this.type == 'multiplication'){
            return 'x'
        }
    }
    checkSum(result){
        return result == (this.first+this.second)
    }
    checkSubtraction(result){
        return result == (this.first-this.second)
    }
    checkMultiplication(result){
        return result == (this.first*this.second)
    }
}
