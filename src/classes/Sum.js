export default class Sum {
    constructor(first,second,id) {
        this.first = first
        this.second = second
        this.id = id
    }

    checkResult(result){
        return result == (this.first+this.second)
    }

}
