import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import basic from './shelves/basic'
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        basic: basic,
    },
    plugins: [createPersistedState()]
})





