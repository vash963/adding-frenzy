import moment from 'moment'

var state = {
    sums: [],
    progressSums:0
}

var actions = {
    setSums ({ commit }, sums) {
        commit('setSumsM', sums)
    },
    clearSumWithId ({ commit }, id) {
        commit('clearSumWithIdM', id)
    },
    clearSums ({ commit }) {
        commit('setSumsM',[])
    },
}

var mutations = {
    setSumsM (state, sums) {
        state.sums = sums
    },
    clearSumWithIdM (state, id) {
        let index = state.sums.findIndex(sum=>{
            return sum.id == id
        })
        state.sums.splice(index,1)
    },
}

var getters = {
    sums: state => {
        return state.sums
    },
}

export default {
    state,
    actions,
    mutations,
    getters
}
